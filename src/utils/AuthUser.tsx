import React, { useState, useContext, useEffect, FC } from 'react'

interface ContextState{
    logged: string;
    setLogged: any;
}
interface AuthUserProps{
    children: any;
}

const AuthUserContext = React.createContext({} as ContextState);

// So we don't conflict with your localhost
const cookieName = 'orrLogged';

export const AuthUserProvider : FC<AuthUserProps> = ({ children }) => {
    const storageLogged = window.localStorage.getItem(cookieName);
    const [logged, setLogged] = useState(storageLogged ? storageLogged: "false")

    useEffect(() => {
        if (logged) {
            window.localStorage.setItem(cookieName, "true");
        } else {
            window.localStorage.remove(cookieName);
        }
    }, [logged])

    return <AuthUserContext.Provider value={{ logged, setLogged }}>{children}</AuthUserContext.Provider>
}

export const useAuthUser = () => {
    return useContext(AuthUserContext)
}