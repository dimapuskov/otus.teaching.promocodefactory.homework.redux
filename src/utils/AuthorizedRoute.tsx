import React, {FC} from 'react'
import { Route, Redirect } from 'react-router-dom'
import { AuthSelect } from '../features/authorize/authorizeSlice';
import {useSelector} from 'react-redux';

interface AuhorizedProps{
    component: any;
    path: string;
}

const AuthorizedRoute: FC<AuhorizedProps> = ({ component, ...rest }) => {

    const authStatus = useSelector(AuthSelect);

    const logged = authStatus.status === "authorized";

    if (logged === null) return <div>Loading...</div>
    if (!logged) return <Redirect push to="/signin" />
    return <Route exact component={component} {...rest} />
}

export default AuthorizedRoute