import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import authorizeSlice from '../features/authorize/authorizeSlice';

export const store = configureStore({
  reducer: {
    authorizer: authorizeSlice,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
